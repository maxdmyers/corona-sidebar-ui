module(..., package.seeall)
----------------------------------------------------------------------------------
--
-- view1.lua
--
----------------------------------------------------------------------------------


--VARS
local modGroup

function loadView(group)
    modGroup = display.newGroup()
    --ADD UI
    local view2Txt = display.newText(modGroup, "View 2", 0,0, 0, 0, native.systemFontBold, 20)
    view2Txt:setReferencePoint(display.CenterReferencePoint)
    view2Txt.x=(screen.right-screen.left)/2
    view2Txt.y=70
    view2Txt:setTextColor(118,118,118)
    
    group:insert(modGroup)
end

function destroyView()
    print('Destroying view...')
    if(modGroup~=nil)then
        modGroup:removeSelf()
        modGroup=nil 
    end
end


